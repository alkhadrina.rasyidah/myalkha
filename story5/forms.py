from django import forms
from .models import Matkul
 
class CreateMatkul(forms.ModelForm):
	class Meta:
		model = Matkul
		fields = ['nama_matkul', 'kelas_matkul']

class DeleteMatkul(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama_matkul']
