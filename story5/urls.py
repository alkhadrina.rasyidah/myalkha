from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('matakuliah/', views.list_matkul, name='list_matkul'),
    path('matakuliah/<str:matkul>/', views.detail_matkul, name='detail_matkul'),
    path('create-matkul/', views.create_matkul, name = 'create_matkul'),
    path('save', views.save),
    path('list_matkul', views.list_matkul),
    path('delete_matkul', views.delete_matkul, name = 'delete_matkul'),
    path('delete', views.delete)
]