from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import CreateMatkul, DeleteMatkul
from .models import Matkul

def list_matkul(request):
    lst = Matkul.objects.all()
    response = {
        "matkul" : lst
    }
    html = 'story5/list_matkul.html'
    return render(request, html, response)

def detail_matkul(request, matkul):
    matkul = Matkul.objects.get(nama_matkul = matkul)
    response = {
        'matkul' : matkul
    }
    html = 'story5/detail_matkul.html'
    return render(request, html, response)

def create_matkul(request):
    response = {
        'create' : CreateMatkul
    }
    html = 'story5/create_matkul.html'
    return render(request, html, response)

def save(request):
    form = CreateMatkul(request.POST or None)
    k = request.POST
    
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/list_matkul')
    else:
        return HttpResponseRedirect('/')

def delete_matkul(request):
    response = {
        'delete' : DeleteMatkul
    }
    html = 'story5/hapus_matkul.html'
    return render(request, html, response)

def delete(request):
    k = request.POST
    obj = Matkul.objects.get(nama_matkul = k['nama_matkul'])
    obj.delete()
    return HttpResponseRedirect('/list_matkul')
