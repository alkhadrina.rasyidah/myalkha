from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth.models import User
# Create your tests here.

class Story9UnitTest(TestCase):
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code , 200)
    
    def test_register_using_register_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response , 'story9/register.html')
    
    def test_register_using_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func , register_request)
    
    def test_form_register_can_be_use(self):
        response = Client().post('/register/', {'username' : 'alkhadrina','email' : 'alkhadrinazahra@gmail.com','password1' : 'areman155', 'password2' : 'areman155'})
        hitung_user = User.objects.all().count()
        self.assertEquals(hitung_user , 1)
    
    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code , 200)
    
    def test_login_using_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response , 'story9/login.html')
    
    def test_login_using_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func , login_request)
    
    def test_form_login_can_be_use(self):
        user = User.objects.create_user(username='alkhadrina', email='alkhadrinazahra@gmail.com', password='areman155')
        response = Client().post('/login/', {'username' : 'alkhadrina','password' : 'areman155'},follow=True)
        self.assertTrue(response.context['user'].is_active)
    
    def test_logout_url_is_exist(self):
        user = User.objects.create_user(username='alkhadrina', email='alkhadrinazahra@gmail.com', password='areman155')
        response = Client().post('/login/', {'username' : 'alkhadrina','password' : 'areman155'},follow=True)
        response2 = Client().get('/logout/')
        self.assertEqual(response.status_code , 200)

    
    

    