from django.test import TestCase, Client
from django.urls import resolve
from .views import kegiatan, form_add_a, form_add_b, form_add_c
from .models import KegiatanA,KegiatanB,KegiatanC
# Create your tests here.

#@tag('story6')
class Story6UnitTest(TestCase):
    def test_kegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code , 200)

    def test_kegiatan_using_kegiatan_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response , 'story6/kegiatan.html')
    
    def test_kegiatan_using_kegiatan_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func , kegiatan)

    def test_form_a_url_is_exist(self):
        response = Client().get('/kegiatan/form-a/')
        self.assertEqual(response.status_code , 200)

    def test_form_a_using_form_a_template(self):
        response = Client().get('/kegiatan/form-a/')
        self.assertTemplateUsed(response , 'story6/form_a.html')
    
    def test_form_a_using_add_a_func(self):
        found = resolve('/kegiatan/form-a/')
        self.assertEqual(found.func , form_add_a)

    def test_model_kegiatan_a_exist(self):
        KegiatanA.objects.create(nama='Zahra')
        hitung_nama = KegiatanA.objects.all().count()
        self.assertEquals(hitung_nama , 1)
    
    def test_form_a_save_and_show(self):
        response = Client().post('/kegiatan/', {'nama_a' : 'Zahra'})
        html_dikembalikan = response.content.decode('utf8')
        self.assertIn("Zahra", html_dikembalikan)
    
    def test_form_a_can_be_use(self):
        response = Client().post('/kegiatan/form-a/', {'nama' : 'Zahra'})
        hitung_nama = KegiatanA.objects.all().count()
        self.assertEquals(hitung_nama , 1)

    def test_form_b_url_is_exist(self):
        response = Client().get('/kegiatan/form-b/')
        self.assertEqual(response.status_code , 200)

    def test_form_b_using_form_b_template(self):
        response = Client().get('/kegiatan/form-b/')
        self.assertTemplateUsed(response , 'story6/form_b.html')
    
    def test_form_b_using_add_b_func(self):
        found = resolve('/kegiatan/form-b/')
        self.assertEqual(found.func , form_add_b)

    def test_model_kegiatan_b_exist(self):
        KegiatanB.objects.create(nama='Zahra')
        hitung_nama = KegiatanB.objects.all().count()
        self.assertEquals(hitung_nama , 1)
    
    def test_form_b_save_and_show(self):
        response = Client().post('/kegiatan/', {'nama_b' : 'Zahra'})
        html_dikembalikan = response.content.decode('utf8')
        self.assertIn("Zahra", html_dikembalikan)
    
    def test_form_b_can_be_use(self):
        response = Client().post('/kegiatan/form-b/', {'nama' : 'Zahra'})
        hitung_nama = KegiatanB.objects.all().count()
        self.assertEquals(hitung_nama , 1)
    
    def test_form_c_url_is_exist(self):
        response = Client().get('/kegiatan/form-c/')
        self.assertEqual(response.status_code , 200)

    def test_form_c_using_form_c_template(self):
        response = Client().get('/kegiatan/form-c/')
        self.assertTemplateUsed(response , 'story6/form_c.html')
    
    def test_form_c_using_add_c_func(self):
        found = resolve('/kegiatan/form-c/')
        self.assertEqual(found.func , form_add_c)

    def test_model_kegiatan_c_exist(self):
        KegiatanC.objects.create(nama='Zahra')
        hitung_nama = KegiatanC.objects.all().count()
        self.assertEquals(hitung_nama , 1)
    
    def test_form_c_save_and_show(self):
        response = Client().post('/kegiatan/', {'nama_c' : 'Zahra'})
        html_dikembalikan = response.content.decode('utf8')
        self.assertIn("Zahra", html_dikembalikan)
    
    def test_form_c_can_be_use(self):
        response = Client().post('/kegiatan/form-c/', {'nama' : 'Zahra'})
        hitung_nama = KegiatanC.objects.all().count()
        self.assertEquals(hitung_nama , 1)
