from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.kegiatan),
    path('kegiatan/form-a/', views.form_add_a),
    path('kegiatan/form-b/', views.form_add_b),
    path('kegiatan/form-c/', views.form_add_c),
]