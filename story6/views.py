from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import KegiatanA,KegiatanB,KegiatanC
from .forms import AddPersonA,AddPersonB,AddPersonC
# Create your views here.

def kegiatan(request):
    nama_a = KegiatanA.objects.all()
    nama_b = KegiatanB.objects.all()
    nama_c = KegiatanC.objects.all()
    response = {
        'nama_a' : nama_a,
        'nama_b' : nama_b,
        'nama_c' : nama_c
    }

    return render(request, 'story6/kegiatan.html', response)

def form_add_a(request):
    form = AddPersonA(request.POST or None)
    response = {
        'form': AddPersonA
    }
    
    k = request.POST
    
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/kegiatan')
    
    return render(request, 'story6/form_a.html', response)

def form_add_b(request):
    response = {
        'form': AddPersonB
    }
    form = AddPersonB(request.POST or None)
    k = request.POST
    
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/kegiatan')
    
    return render(request, 'story6/form_b.html', response)

def form_add_c(request):
    response = {
        'form': AddPersonC
    }
    form = AddPersonC(request.POST or None)
    k = request.POST
    
    if(form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/kegiatan')
    
    return render(request, 'story6/form_c.html', response)
