from django import forms
from .models import KegiatanA,KegiatanB,KegiatanC

class AddPersonA(forms.ModelForm):
    class Meta:
        model = KegiatanA
        fields = ['nama']
    def clean_nama(self):
        nama = self.cleaned_data.get('nama')
        try:
            match = KegiatanA.objects.get(nama=nama)
        except KegiatanA.DoesNotExist:
            # Unable to find a user, this is fine
            return nama

        # A user was found with this as a username, raise an error.
        raise forms.ValidationError('This name is already in use.')

class AddPersonB(forms.ModelForm):
    class Meta:
        model = KegiatanB
        fields = ['nama']
    def clean_nama(self):
        nama = self.cleaned_data.get('nama')
        try:
            match = KegiatanB.objects.get(nama=nama)
        except KegiatanB.DoesNotExist:
            # Unable to find a user, this is fine
            return nama

        # A user was found with this as a username, raise an error.
        raise forms.ValidationError('This name is already in use.')

class AddPersonC(forms.ModelForm):
    class Meta:
        model = KegiatanC
        fields = ['nama']
    def clean_nama(self):
        nama = self.cleaned_data.get('nama')
        try:
            match = KegiatanC.objects.get(nama=nama)
        except KegiatanC.DoesNotExist:
            # Unable to find a user, this is fine
            return nama

        # A user was found with this as a username, raise an error.
        raise forms.ValidationError('This name is already in use.')
