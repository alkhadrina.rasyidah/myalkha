from django.test import TestCase, Client
from django.urls import resolve
from .views import *
# Create your tests here.

class Story8UnitTest(TestCase):
    def test_search_url_is_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code , 200)
    
    def test_search_using_search_template(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response , 'story8/search.html')
    
    def test_search_using_search_func(self):
        found = resolve('/search/')
        self.assertEqual(found.func , search)

    def test_data_url_is_exist(self):
        response = Client().get('/data/?q=website/')
        self.assertEqual(response.status_code , 200)