from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

def search(request):
    response={}
    return render(request,"story8/search.html",response)

def data(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q='+ request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data,safe=False)